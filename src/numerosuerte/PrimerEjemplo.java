/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerosuerte;

import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class PrimerEjemplo {

    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        new PrimerEjemplo().calcularNumeroSuerte();

        new PrimerEjemplo().calcularNumeroPerfecto();
    }

    public void calcularNumeroSuerte() {
        Scanner sc = new Scanner(System.in);
        int dia, mes, año, numeroSuerte, sumaFecha, numero1, numero2, numero3, numero4;
        System.out.println("Introduzca fecha de nacimiento");
        System.out.print("Día: ");
        dia = sc.nextInt();
        System.out.print("Mes: ");
        mes = sc.nextInt();
        System.out.print("Año: ");
        año = sc.nextInt();
        sumaFecha = dia + mes + año;
        numero1 = sumaFecha/1000;      
        numero2 = sumaFecha/100%10;  
        numero3 = sumaFecha/10%10; 
        numero4 = sumaFecha%10;   
        numeroSuerte = numero1 + numero2 + numero3 + numero4;
        System.out.println("Su número de la suerte es: " + numeroSuerte);

    }

    public void calcularNumeroPerfecto() {
        int i, sumaNumero = 0, n;
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce un número: ");
        n = sc.nextInt();
        for (i = 1; i < n; i++) {  // i son los divisores. Se divide desde 1 hasta n-1 
            if (n % i == 0) {
                sumaNumero = sumaNumero + i;     // si es divisor se suma
            }
        }
        if (sumaNumero == n) {  // si el numero es igual a la suma de sus divisores es perfecto
            System.out.println("Es perfecto!!!");
        } else {
            System.out.println("No es perfecto!!!");

        }
    }
}
